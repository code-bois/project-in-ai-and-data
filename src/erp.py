# Fagprojekt
# ERP testing
#from os import listdir, path
#import os
#path = "/Users/kathr/Documents/Fagprojekt/project-in-ai-and-data/src"
#os.chdir(path)
# Load dependencies
from os import chdir, path
from data_load import getData
import matplotlib.pyplot as plt # Plot data
import numpy as np              # Numpy array features
from os import listdir, path    # OS pathing functions
from scipy.io import loadmat    # Load matlab data file
from utilities import get_cycle # Fancy color map for plots
from matplotlib.lines import Line2D
import pandas as pd


def runTest(params, TA_path = "local_data/EEG/"):
    """
    Input:
        params      = List of parameters for the test
        TA_path     = Path for the .mat files for the TAs (default: local_data/EEG/)

    Runs the ERP to test for triggers
    """

    # Initial setup
    full_analysis = params["full"]
    conditions = params["conditions"]
    TA_list = listdir(TA_path)                           # Lists all .mat files in the TA_path
    TA_list = TA_list if full_analysis else [TA_list[0]] # Take the first TA if not running full analysis

    print("Running %s analysis..." %("full" if full_analysis else "quick"))

    df_list = []

    i = 0
    for TA in TA_list:

        print("Analysis on: %s" %TA)

        file_EEG = TA_path + TA
        #global data # Debugging
        data = loadmat(file_EEG)     # Load the .mat file
        data = data["trial_cond"][0] # Remove the global, version and header entries of the dict from loadmat()

        #colors = ["red", "blue", "green"]
        #fig, axes = plt.subplots(3, 3, figsize = (15, 10))


        for cond in conditions:
            sub_data = data[cond][0][0]                # Select sub data, per trial condition, i.e. make it easier from here
            n_trials = len(sub_data["trial"][0])       # Get number of trials
            fsample = sub_data["fsample"][0][0]        # Get the sample rate
            labels = sub_data["label"][0][0]           # Get electrode label names
            n_channels = len(labels)                   # Number of electrodes (channels)

            data_target_on = []
            data_mask_on = []
            data_audio_off = []

            t_target_on = [-1,1]
            t_mask_on = [-1,1]
            t_audio_off = [-1,1]

            for trial in range(n_trials):
                # Define the time axis (in seconds) and delay
                t = sub_data["time"][0][trial][0]
                mask_delay = sub_data["trialinfo"][trial][2] # Delay between target and mask speech
                mask_delay_s = mask_delay / fsample

                target_on = (t >= t_target_on[0]) & (t < t_target_on[1])
                mask_on   = (t >= mask_delay_s + t_mask_on[0]) & (t < mask_delay_s + t_mask_on[1])
                audio_off = (t >= 60 + t_audio_off[0]) & (t < 60 + t_audio_off[1])

                """
                To get the EEG data, use the following:
                    sub_data["trial"][0][trial][idx][EEG_mask][:t_length]
                where idx is the channel index (0-15 for 16 channels)
                """

                # Compute mean
                trial_mean = np.mean(sub_data["trial"][0][trial], axis = 0)

                # Append data
                data_target_on.append(trial_mean[target_on])
                data_mask_on.append(trial_mean[mask_on])
                data_audio_off.append(trial_mean[audio_off])

            # Compute mean of trials
            mean_target_on = np.mean(data_target_on, axis = 0)
            mean_mask_on   = np.mean(data_mask_on, axis = 0)
            mean_audio_off = np.mean(data_audio_off, axis = 0)

            df_list.append([i, cond, "target_on"] + list(mean_target_on))
            df_list.append([i, cond, "mask_on"] + list(mean_mask_on))
            df_list.append([i, cond, "audio_off"] + list(mean_audio_off))
        i += 1
    # Plot the results here
    return df_list


if __name__ == "__main__":
    params = {
        "full" : True,          # Run a full analysis on all TAs
        "conditions" : [0, 1, 2] # Trial conditions, 0 = -5, 1 = 0 and 2 = +5 SNR
    }
    df = runTest(params)
    df = np.asarray(df)

    df = pd.DataFrame(data = df)

    df[0] = df[0].astype("category")
    df[1] = df[1].astype("category")
    df[2] = df[2].astype("category")

    for i in range(512):
        df[i+3] = df[i+3].astype("float64")

    df["TA"] = df[0]
    df["SNR"] = df[1]
    df["event"] = df[2]

    df = df[df.columns[3:,]]

    df_grouped = df.groupby(["SNR", "event"]).mean().reset_index()

    colors = ["red", "blue", "green"]
    plt.style.use("seaborn-darkgrid")

    SNRs = np.unique(df_grouped["SNR"])

    fig, axes = plt.subplots(3, 3, figsize = (15, 10))

    i = 0
    for SNR in SNRs:
        #events = np.unique(df_grouped["event"])
        events = ["target_on", "mask_on", "audio_off"]
        j = 0
        for event in events:
            sub_data = df[(df["SNR"] == SNR) & (df["event"] == event)]
            sub_data_grouped = df_grouped[(df_grouped["SNR"] == SNR) & (df_grouped["event"] == event)]
            for k in range(len(sub_data)):
                y_values = sub_data.iloc[k,:512]
                x_values = np.linspace(-1, 1, len(y_values))
                axes[i, j].plot(x_values, y_values, color = ".6")
            y_values = sub_data_grouped.iloc[0,2:]
            x_values = np.linspace(-1, 1, len(y_values))
            axes[i, j].plot(x_values, y_values, color = colors[int(SNR)])
            axes[i, j].axvline(0, color = ".3")
            if i == 0:
                axes[i, j].set_title(event.replace("_", " ").capitalize())
            if i == 2:
                axes[i, j].set_xlabel("Time difference to event (s)")
            #if j == 0:
                #axes[i, j].set_ylabel()
            j += 1
        i += 1
    lines = [Line2D([0], [0], color = colors[0], lw=4),
             Line2D([0], [0], color = colors[1], lw=4),
             Line2D([0], [0], color = colors[2], lw=4)]
    fig.legend(lines, ["-5 dB SNR", "0 dB SNR", "+5 dB SNR"], loc = "center right", borderaxespad=0.)
    fig.suptitle("ERP on all trials", fontsize = 16)
    plt.show()

    """
    for i in range(len(df_grouped)):
        plt.plot(list(df_grouped.iloc[i]))
        plt.show()
    """
    #df = df.rename({0 : "TA", 1: "SNR", 2: "event"})
