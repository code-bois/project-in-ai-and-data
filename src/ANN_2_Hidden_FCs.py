  #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Predict speech envelope from EEG using ANN

Build ANN model with 1 hidden layer using Keras for every SNR value; I.e., for -5, 0 & 5 DB, respectively. 
Trains using leave-trial-out CV.
Pearson's R correlation coefficient is used as loss function; and for validation.
    
"""

# Load dependencies
from os import path, chdir
from sklearn.model_selection import LeaveOneGroupOut
from data_load import getData, random_trial
import numpy as np
import pandas as pd
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
from keras.optimizers import Adam
import seaborn as sns
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras import backend as K
from keras.layers.normalization import BatchNormalization

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# Pearson R correlation Loss function for ANN model from Taillez et al (2017)
# https://github.com/tdetaillez/neural_networks_auditory_attention_decoding
def corr_loss(act,pred):          
    cov=(K.mean((act-K.mean(act))*(pred-K.mean(pred))))
    return 1-(cov/(K.std(act)*K.std(pred)+K.epsilon()))

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

# Modified above function to use for Pearson R correlation coefficient calculation
def corr(act,pred):          
    cov=(np.mean((act-np.mean(act))*(pred-np.mean(pred))))
    return (cov/(np.std(act)*np.std(pred)))

def cross_validate(data):
    
    """

    Parameters
    ----------
    data : Pandas Dataframe
        load dataframe using from data_load, using getData function.
    Returns
    -------
    Pickle file with dataframe of results;

    """

    # Split into train/testing with leave-one-group-out
    logo = LeaveOneGroupOut()

    # Define result DataFrame
    df_cols = ["corr_true", "corr_mask", "corr_rand", "TA", "SNR"]
    df = pd.DataFrame(columns = df_cols)

    TAs = np.unique(data["TA"])

    for TA in TAs:
        SNRs = np.unique(data[data["TA"] == TA]["SNR"])

        for SNR in SNRs:
            data_sub = data[(data["TA"] == TA) & (data["SNR"] == SNR)]

            # Assign X, y and group variable (trial, as to do leave-trial-out)
            X = data_sub[data.columns[:16]]
            
            # Attended audio
            y = data_sub["target"]
            
            # Unattended audio
            masks = data_sub["mask"]
            
            groups = data_sub["trial"]
            n_outer_groups = len(np.unique(groups))

            ### Two-layer CV starts here ###
            # Outer fold
            i = 0
            for out_train_idx, out_test_idx in logo.split(X, y, groups):
                print("TA = %i / %i\tSNR = %i / %i\nFold %i / %i" %(TA + 1, len(TAs), SNR + 1, len(SNRs), i + 1, n_outer_groups))
                X_train = X.iloc[out_train_idx]
                y_train = y.iloc[out_train_idx]

                X_test = X.iloc[out_test_idx]
                y_test = y.iloc[out_test_idx]
                
                # Number of hidden nodes in first hidden layer
                node_opt = 16

                # Train optimal model               
                model_opt = Sequential()
                model_opt.add(Dense(units=node_opt, activation='tanh',input_shape=(16,)))
                
                # Batch normalization
                model_opt.add(BatchNormalization())
                model_opt.add(Dense(units=2, activation='tanh'))
                
                # Batch normalization
                model_opt.add(BatchNormalization())
                model_opt.add(Dense(units=1, activation='linear'))
                opt = Adam(learning_rate=0.001)
                model_opt.compile(optimizer=opt, loss=corr_loss)
                model_opt.fit(np.asarray(X_train), np.asarray(y_train), epochs=30, verbose=2, shuffle=True)     

                # Predict envelope
                y_pred = model_opt.predict(np.asarray(X_test))
                
                """
                plt.style.use('ggplot')
                plt.plot(y_test, label="True value")
                plt.plot(y_pred, label="Predicted value")
                plt.legend()
                plt.show()
                """

                trial_test = np.unique(data_sub.iloc[out_test_idx]["trial"])[0]
                
                # Random speech 
                y_rand = random_trial(data, TA = TA, trial = trial_test)["target"]
                
                # Compute Pearson R between predicted envelope and attended speech
                corr_true = corr(np.asarray(y_test).reshape(-1, 1),y_pred)
                
                # Compute Pearson R between predicted envelope and unattended speech
                corr_mask = corr(np.asarray(masks.iloc[out_test_idx]).reshape(-1, 1),y_pred)
                
                # Compute Pearson R between predicted envelope and random speech
                corr_rand = corr(np.asarray(y_rand).reshape(-1, 1),y_pred)

                # Evaluate envelope, compare with random trial
                ### Add correlations to dataframe ###
                # Convert to DataFrame
                data_results = np.zeros((1, len(df_cols)))
                data_results[:, 0] = corr_true
                data_results[:, 1] = corr_mask
                data_results[:, 2] = corr_rand
                data_results[:, 3] = TA
                data_results[:, 4] = SNR
                print('true')
                print(corr_true)
                print('mask')
                print(corr_mask)
                print('rand')
                print(corr_rand)
                df_ = pd.DataFrame(data = data_results, columns = df_cols)

                # Concatenate
                df = pd.concat([df, df_], ignore_index = True)
                print(df)

                i += 1
            df.to_pickle("/content/Measuring_Cognitive_Load_DTU_WSAudiology/local_data/results/Seperate_SNR_2_layer_ANN_result_%i_%i.pkl" %(TA, SNR))
    return df


if __name__ == "__main__":
    # Set working directory
    chdir(path.dirname(__file__))

    # Get the data
    data = getData()

    # Run the script
    results = cross_validate(data)

    #results = pd.read_pickle("local_data/results_all_SNR.pkl")
    
    # Classification plots for all conditions:
    """
    results_true = results[results["corr_true"] >= results["corr_mask"]]
    results_false = results[results["corr_true"] < results["corr_mask"]]

    xrange = np.array([min(results["corr_mask"]), max(results["corr_mask"])])
    yrange = np.array([min(results["corr_true"]), max(results["corr_true"])])
    plt.plot([-1,1], [-1,1], alpha = .3, color = "black")
    
    plt.scatter(results_true["corr_mask"], results_true["corr_true"], label = "True")
    plt.scatter(results_false["corr_mask"], results_false["corr_true"], label = "False")

    plt.xlim(xrange * 1.1)
    plt.ylim(yrange * 1.1)
    plt.xlabel("Mask Speech r-values")
    plt.ylabel("Target Speech r-values")
    plt.title("All Conditions, Accuracy:" + " "+f"{int(len(results_true) * 100 / len(results))}%")
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()

    results_true = results[results["corr_true"] >= results["corr_rand"]]
    results_false = results[results["corr_true"] < results["corr_rand"]]

    xrange = np.array([min(results["corr_rand"]), max(results["corr_rand"])])
    yrange = np.array([min(results["corr_true"]), max(results["corr_true"])])
    plt.plot([-1,1], [-1,1], alpha = .3, color = "black")

    plt.scatter(results_true["corr_rand"], results_true["corr_true"], label = "True")
    plt.scatter(results_false["corr_rand"], results_false["corr_true"], label = "False")

    plt.xlim(xrange * 1.1)
    plt.ylim(yrange * 1.1)
    plt.xlabel("Random Speech r-values")
    plt.ylabel("Target Speech r-values")
    plt.title("All Conditions, Accuracy:" + " "+f"{int(len(results_true) * 100 / len(results))}%")
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()
    """
    
    for i in range(3):
        # Extract points
        results_sub = results[results["SNR"] == i]
        results_true = results_sub[results_sub["corr_true"] >= results_sub["corr_mask"]]
        results_false = results_sub[results_sub["corr_true"] < results_sub["corr_mask"]]
        # Plot diagonal line
        xrange = np.array([min(results_sub["corr_mask"]), max(results_sub["corr_mask"])])
        yrange = np.array([min(results_sub["corr_true"]), max(results_sub["corr_true"])])
        plt.plot([-1,1], [-1,1], alpha = .3, color = "black")
        # Plot the points
        plt.scatter(results_true["corr_mask"], results_true["corr_true"], label = "True")
        plt.scatter(results_false["corr_mask"], results_false["corr_true"], label = "False")
        # Limit the plot
        plt.xlim(xrange * 1.1)
        plt.ylim(yrange * 1.1)
        plt.xlabel("Mask corrcoef")
        plt.ylabel("Target corrcoef")
        plt.title("SNR: %i dB, Accuracy: %i%%" %(i * 5 - 5, len(results_true) * 100 / len(results_sub)))
        plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
        plt.show()

    for i in range(3):
        # Extract points
        results_sub = results[results["SNR"] == i]
        results_true = results_sub[results_sub["corr_true"] >= results_sub["corr_rand"]]
        results_false = results_sub[results_sub["corr_true"] < results_sub["corr_rand"]]
        # Plot diagonal line
        xrange = np.array([min(results_sub["corr_rand"]), max(results_sub["corr_rand"])])
        yrange = np.array([min(results_sub["corr_true"]), max(results_sub["corr_true"])])
        plt.plot([-1,1], [-1,1], alpha = .3, color = "black")
        # Plot the points
        plt.scatter(results_true["corr_rand"], results_true["corr_true"], label = "True")
        plt.scatter(results_false["corr_rand"], results_false["corr_true"], label = "False")
        # Limit the plot
        plt.xlim(xrange * 1.1)
        plt.ylim(yrange * 1.1)
        plt.xlabel("Random corrcoef")
        plt.ylabel("Target corrcoef")
        plt.title("SNR: %i dB, Accuracy: %i%%" %(i * 5 - 5, len(results_true) * 100 / len(results_sub)))
        plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
        plt.show()


    # Transform SNR to dB equivalent
    results["SNR"] = results["SNR"] * 5 - 5
    results["SNR"] = results["SNR"].astype("category")
    results["TA"] = results["TA"].astype("category")
    results = results.groupby(['SNR', 'TA']).mean().reset_index()
    # Final transformations
    dd=pd.melt(results,id_vars=['SNR','TA'],value_vars=['corr_true','corr_mask','corr_rand'],var_name='envelopes')
    dd["envelopes"] = dd["envelopes"].replace("corr_true","Target")
    dd["envelopes"] = dd["envelopes"].replace("corr_mask","Mask")
    dd["envelopes"] = dd["envelopes"].replace("corr_rand","Random")

    sns.set(style="darkgrid")
    # Boxplots per SNR
    sns.boxplot(x='SNR',y='value',data=dd,hue='envelopes')
    plt.xlabel("SNR (dB)")
    plt.ylabel("Score (corrcoef)")
    plt.title("Boxplot of results")
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()

    # Per TA:
    sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Target"], label="Target")
    sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Mask"], label="Mask")
    sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Random"], label="Random")
    plt.xlabel("TS")
    plt.ylabel("Score (corrcoef)")
    plt.xticks(np.arange(7),np.arange(7)+1)
    plt.title("TS Performance")
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()
