#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Predict speech envelope from EEG using ANN

Build single ANN model with 2 hidden layer using Keras for all SNR values; I.e., -5, 0 & 5 DB. 
Trains using leave-trial-out CV.
Pearson's R correlation coefficient is used as loss function; and for validation.
    
"""

# Load dependencies
from os import path, chdir
from data_load import getData, random_trial
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam
from keras import backend as K
from keras.layers.normalization import BatchNormalization
import matplotlib.pyplot as plt

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# Pearson R correlation Loss function for ANN model from Taillez et al (2017)
# https://github.com/tdetaillez/neural_networks_auditory_attention_decoding
def corr_loss(act,pred):          
    cov=(K.mean((act-K.mean(act))*(pred-K.mean(pred))))
    return 1-(cov/(K.std(act)*K.std(pred)+K.epsilon()))

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

# Modified above function to use for Pearson R correlation coefficient calculation
def corr(act,pred):          
    cov=(np.mean((act-np.mean(act))*(pred-np.mean(pred))))
    return (cov/(np.std(act)*np.std(pred)))

def cross_validate(data, TA = None):
    """

    Parameters
    ----------
    data : Pandas Dataframe
        load dataframe using from data_load, using getData function.

    Returns
    -------
    Pickle file with dataframe of results;

    """

    np.random.seed(999)

    # Define result DataFrame
    df_cols = ["corr_true", "corr_mask", "corr_rand", "TA", "SNR"]
    df = pd.DataFrame(columns = df_cols)

    if TA == None:
        TAs = np.unique(data["TA"])
    else:
        TAs = np.array([TA])

    for TA in TAs:
        data_sub = data[data["TA"] == TA]
        data_train = data_sub

        SNRs = np.unique(data_sub["SNR"])
        SNR_order = []

        for SNR in SNRs:
            trials = data_sub[data_sub["SNR"] == SNR]["trial"] # Get the trials
            trials = np.unique(trials) # Get the unique trial indicies
            np.random.shuffle(trials) # Shuffle the order of the trials
            SNR_order.append(trials) # Store the order

        # Get the lowest possible k for k-fold
        H = np.inf
        for order in SNR_order:
            if len(order) < H:
                H = len(order)

        # Outer fold
        for k in range(H):
            print("TA: %i / %i\n\tFold: %i / %i" %(TA + 1, len(TAs), k + 1, H))
            # Split into test and training
            data_train = data_sub
            data_test = pd.DataFrame()
            # Filter the test data away
            for i in range(len(SNR_order)):
                data_test = pd.concat([data_test, data_train[(data_sub["SNR"] == i) & (data_train["trial"] == SNR_order[i][k])]], ignore_index = True)
                data_train = data_train.drop(data_train[(data_train["SNR"] == i) & (data_train["trial"] == SNR_order[i][k])].index)

            # Get the list of validation trials
            SNR_valid_order = SNR_order.copy()
            for i in range(len(SNR_order)):
                SNR_valid_order[i] = np.delete(SNR_valid_order[i],k)
            
            # Number of nodes in first hidden layer
            node_opt = 16

            # Train optimal model        
            model_opt = Sequential()
            model_opt.add(Dense(units=node_opt, activation='tanh',input_shape=(16,)))
                
            # Batch normalization
            model_opt.add(BatchNormalization())
            model_opt.add(Dense(units=2, activation='tanh'))
                
            # Batch normalization
            model_opt.add(BatchNormalization())
            model_opt.add(Dense(units=1, activation='linear'))
            opt = Adam(learning_rate=0.001)
            model_opt.compile(optimizer=opt, loss=corr_loss)

            # Fit model to train data
            model_opt.fit(np.asarray(data_train[data.columns[:16]]), np.asarray(data_train["target"]), epochs=35, verbose=2, shuffle=True,batch_size=64)     
            for i in range(len(SNR_order)):
                # Predict envelope
                data_test_SNR = data_test[data_test["SNR"] == i]
                y_pred = model_opt.predict(np.asarray(data_test_SNR[data.columns[:16]]))
                y_rand = random_trial(data, TA = TA, trial = SNR_order[i][k])["target"]

                # Compute Pearson R between predicted envelope and attended speech
                corr_true = corr(np.asarray(data_test_SNR["target"]).reshape(-1, 1),y_pred)
                
                # Compute Pearson R between predicted envelope and unattended speech
                corr_mask = corr(np.asarray(data_test_SNR["mask"]).reshape(-1, 1),y_pred)
                
                # Compute Pearson R between predicted envelope and random speech
                corr_rand = corr(np.asarray(y_rand).reshape(-1, 1),y_pred)

                # Convert to DataFrame
                data_results = np.zeros((1, len(df_cols)))
                data_results[:, 0] = corr_true
                data_results[:, 1] = corr_mask
                data_results[:, 2] = corr_rand
                data_results[:, 3] = TA
                data_results[:, 4] = i
                print('true')
                print(corr_true)
                print('mask')
                print(corr_mask)
                print('rand')
                print(corr_rand)

                df_ = pd.DataFrame(data = data_results, columns = df_cols)

                # Concatenate
                df = pd.concat([df, df_], ignore_index = True)
                print(df)

            df.to_pickle("/content/Measuring_Cognitive_Load_DTU_WSAudiology/local_data/results/ALL_SNR_2_layer_ANN_result_%i_%i.pkl" %(TA, k))
    print("Done")
    return df

if __name__ == "__main__":
    # Set working directory
    chdir(path.dirname(__file__))

    # Get the data
    data = getData()

    # Run the script
    results = cross_validate(data)

    #results = pd.read_pickle("local_data/results_all_SNR.pkl")
    
    # Classification plots for all conditions:
    """
    results_true = results[results["corr_true"] >= results["corr_mask"]]
    results_false = results[results["corr_true"] < results["corr_mask"]]

    xrange = np.array([min(results["corr_mask"]), max(results["corr_mask"])])
    yrange = np.array([min(results["corr_true"]), max(results["corr_true"])])
    plt.plot([-1,1], [-1,1], alpha = .3, color = "black")
    
    plt.scatter(results_true["corr_mask"], results_true["corr_true"], label = "True")
    plt.scatter(results_false["corr_mask"], results_false["corr_true"], label = "False")

    plt.xlim(xrange * 1.1)
    plt.ylim(yrange * 1.1)
    plt.xlabel("Mask Speech r-values")
    plt.ylabel("Target Speech r-values")
    plt.title("All Conditions, Accuracy:" + " "+f"{int(len(results_true) * 100 / len(results))}%")
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()

    results_true = results[results["corr_true"] >= results["corr_rand"]]
    results_false = results[results["corr_true"] < results["corr_rand"]]

    xrange = np.array([min(results["corr_rand"]), max(results["corr_rand"])])
    yrange = np.array([min(results["corr_true"]), max(results["corr_true"])])
    plt.plot([-1,1], [-1,1], alpha = .3, color = "black")

    plt.scatter(results_true["corr_rand"], results_true["corr_true"], label = "True")
    plt.scatter(results_false["corr_rand"], results_false["corr_true"], label = "False")

    plt.xlim(xrange * 1.1)
    plt.ylim(yrange * 1.1)
    plt.xlabel("Random Speech r-values")
    plt.ylabel("Target Speech r-values")
    plt.title("All Conditions, Accuracy:" + " "+f"{int(len(results_true) * 100 / len(results))}%")
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()
    """

    for i in range(3):
        # Extract points
        results_sub = results[results["SNR"] == i]
        results_true = results_sub[results_sub["corr_true"] >= results_sub["corr_mask"]]
        results_false = results_sub[results_sub["corr_true"] < results_sub["corr_mask"]]
        # Plot diagonal line
        xrange = np.array([min(results_sub["corr_mask"]), max(results_sub["corr_mask"])])
        yrange = np.array([min(results_sub["corr_true"]), max(results_sub["corr_true"])])
        plt.plot([-1,1], [-1,1], alpha = .3, color = "black")
        # Plot the points
        plt.scatter(results_true["corr_mask"], results_true["corr_true"], label = "True")
        plt.scatter(results_false["corr_mask"], results_false["corr_true"], label = "False")
        # Limit the plot
        plt.xlim(xrange * 1.1)
        plt.ylim(yrange * 1.1)
        plt.xlabel("Mask corrcoef")
        plt.ylabel("Target corrcoef")
        plt.title("SNR: %i dB, Accuracy: %i%%" %(i * 5 - 5, len(results_true) * 100 / len(results_sub)))
        plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
        plt.show()

    for i in range(3):
        # Extract points
        results_sub = results[results["SNR"] == i]
        results_true = results_sub[results_sub["corr_true"] >= results_sub["corr_rand"]]
        results_false = results_sub[results_sub["corr_true"] < results_sub["corr_rand"]]
        # Plot diagonal line
        xrange = np.array([min(results_sub["corr_rand"]), max(results_sub["corr_rand"])])
        yrange = np.array([min(results_sub["corr_true"]), max(results_sub["corr_true"])])
        plt.plot([-1,1], [-1,1], alpha = .3, color = "black")
        # Plot the points
        plt.scatter(results_true["corr_rand"], results_true["corr_true"], label = "True")
        plt.scatter(results_false["corr_rand"], results_false["corr_true"], label = "False")
        # Limit the plot
        plt.xlim(xrange * 1.1)
        plt.ylim(yrange * 1.1)
        plt.xlabel("Random corrcoef")
        plt.ylabel("Target corrcoef")
        plt.title("SNR: %i dB, Accuracy: %i%%" %(i * 5 - 5, len(results_true) * 100 / len(results_sub)))
        plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
        plt.show()


    # Transform SNR to dB equivalent
    results["SNR"] = results["SNR"] * 5 - 5
    results["SNR"] = results["SNR"].astype("category")
    results["TA"] = results["TA"].astype("category")
    results = results.groupby(['SNR', 'TA']).mean().reset_index()
    # Final transformations
    dd=pd.melt(results,id_vars=['SNR','TA'],value_vars=['corr_true','corr_mask','corr_rand'],var_name='envelopes')
    dd["envelopes"] = dd["envelopes"].replace("corr_true","Target")
    dd["envelopes"] = dd["envelopes"].replace("corr_mask","Mask")
    dd["envelopes"] = dd["envelopes"].replace("corr_rand","Random")

    sns.set(style="darkgrid")
    # Boxplots per SNR
    sns.boxplot(x='SNR',y='value',data=dd,hue='envelopes')
    plt.xlabel("SNR (dB)")
    plt.ylabel("Score (corrcoef)")
    plt.title("Boxplot of results")
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()

    # Line plots per TA
    sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Target"], label="Target")
    sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Mask"], label="Mask")
    sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Random"], label="Random")
    plt.xlabel("TS")
    plt.ylabel("Score (corrcoef)")
    plt.xticks(np.arange(7),np.arange(7)+1)
    plt.title("TS Performance")
    plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()
