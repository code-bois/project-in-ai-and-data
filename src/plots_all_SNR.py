
# Load dependencies
from os import path, chdir
from sklearn.model_selection import LeaveOneGroupOut
from data_load import getData, random_trial
import numpy as np
from mne.decoding import ReceptiveField
from scipy import signal

import pandas as pd
from scipy.stats import pearsonr
import matplotlib.pyplot as plt
import pickle

import seaborn as sns


results = pd.read_pickle("C:\\Users\\kathr\\Documents\\Fagprojekt\\project-in-ai-and-data\\src\\local_data\\results_all_SNR.pkl")

for i in range(3):
	# Extract points
	#plt.style.use('seaborn')
	results_sub = results[results["SNR"] == i]
	results_true = results_sub[results_sub["corr_true"] >= results_sub["corr_mask"]]
	results_false = results_sub[results_sub["corr_true"] < results_sub["corr_mask"]]
	# Plot diagonal line
	xrange = np.array([min(results_sub["corr_mask"]), max(results_sub["corr_mask"])])
	yrange = np.array([min(results_sub["corr_true"]), max(results_sub["corr_true"])])
	plt.plot([-1,1], [-1,1], alpha = .3, color = "black")
	# Plot the points
	plt.scatter(results_true["corr_mask"], results_true["corr_true"], label = "True")
	plt.scatter(results_false["corr_mask"], results_false["corr_true"], label = "False")
	# Limit the plot
	plt.xlim(xrange * 1.1)
	plt.ylim(yrange * 1.1)
	plt.xlabel("Mask corrcoef")
	plt.ylabel("Target corrcoef")
	plt.title("SNR: %i dB, Accuracy: %i%%" %(i * 5 - 5, len(results_true) * 100 / len(results_sub)))
	plt.legend(loc='upper right', borderaxespad=0.)
	plt.show()

for i in range(3):
	# Extract points
	results_sub = results[results["SNR"] == i]
	results_true = results_sub[results_sub["corr_true"] >= results_sub["corr_rand"]]
	results_false = results_sub[results_sub["corr_true"] < results_sub["corr_rand"]]
	# Plot diagonal line
	xrange = np.array([min(results_sub["corr_rand"]), max(results_sub["corr_rand"])])
	yrange = np.array([min(results_sub["corr_true"]), max(results_sub["corr_true"])])
	plt.plot([-1,1], [-1,1], alpha = .3, color = "black")
	# Plot the points
	plt.scatter(results_true["corr_rand"], results_true["corr_true"], label = "True")
	plt.scatter(results_false["corr_rand"], results_false["corr_true"], label = "False")
	# Limit the plot
	plt.xlim(xrange * 1.1)
	plt.ylim(yrange * 1.1)
	plt.xlabel("Random corrcoef")
	plt.ylabel("Target corrcoef")
	plt.title("SNR: %i dB, Accuracy: %i%%" %(i * 5 - 5, len(results_true) * 100 / len(results_sub)))
	plt.legend(loc='upper right', borderaxespad=0.)
	plt.show()


# Transform SNR to dB equivalent
results["SNR"] = results["SNR"] * 5 - 5
results["SNR"] = results["SNR"].astype("category")
results["TA"] = results["TA"].astype("category")
results = results.groupby(['SNR', 'TA']).mean().reset_index()
# Final transformations
dd=pd.melt(results,id_vars=['SNR','TA'],value_vars=['corr_true','corr_mask','corr_rand'],var_name='envelopes')
dd["envelopes"] = dd["envelopes"].replace("corr_true","Target")
dd["envelopes"] = dd["envelopes"].replace("corr_mask","Mask")
dd["envelopes"] = dd["envelopes"].replace("corr_rand","Random")

sns.set(style="darkgrid")
# Boxplots per SNR
sns.boxplot(x='SNR',y='value',data=dd,hue='envelopes')
plt.xlabel("SNR (dB)")
plt.ylabel("Score (corrcoef)")
plt.title("Boxplot of Correlations")
plt.legend(loc='upper right', borderaxespad=0.)
plt.show()

# Line plots per TA
sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Target"], label="Target")
sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Mask"], label="Mask")
sns.lineplot(x='TA',y='value',data=dd[dd["envelopes"]=="Random"], label="Random")
plt.xlabel("TS")
plt.ylabel("Score (corrcoef)")
plt.xticks(np.arange(7),np.arange(7)+1)
plt.title("TS Performance")
plt.legend(loc='upper right', borderaxespad=0.)
plt.show()