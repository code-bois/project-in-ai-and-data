#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 13:19:41 2020

@author: hso
"""
# Import dependencies
from os import chdir, path
from data_load import getData
from scipy import signal
from signal_processing import normalize, shift
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.dummy import DummyClassifier
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct, RBF
from data_load import getData
from sklearn.model_selection import KFold
from sklearn import tree

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from scipy.stats import pearsonr
import pickle
import seaborn as sns


def transform_to_SNR(data):
    """
    Input:
        data = Dataframe from dataLoad()

    Output:
        df   = Transformed dataframe with each trial as a single sample point
    """
    new_cols = ["trial", "SNR", "TA"]
    df = pd.DataFrame(columns = new_cols)
    df["trial"] = df["trial"].astype(object)
    conditions = np.unique(data["SNR"])
    TAs = np.unique(data["TA"])
    print("Transforming data...")
    for TA in TAs:
        print("TA: %i / %i" %(TA + 1, len(TAs)))
        for cond in conditions:
            print("\tCondition: %i / %i" %(cond + 1, len(conditions)))
            TA_data = data[(data["SNR"] == cond) & (data["TA"] == TA)]
            trials = np.unique(TA_data["trial"])
            for trial in trials:
                temp_data = []
                sample = TA_data[TA_data["trial"] == trial]

                # shift target 150 ms back
                EEG_shifted = shift(sample[sample.columns[:16]].T, lag = -150, freq = 64)
                sample = sample.iloc[:len(EEG_shifted.T)]
                sample[sample.columns[:16]] = EEG_shifted.T

                temp_row=[]

                for i in range(0,16):
                    #crosscor = np.mean(signal.correlate(sample[data.columns[i]],sample['target'], mode = 'same')/len(sample['target']))
                    corrcoef = pearsonr(sample[data.columns[i]], sample["target"])[0]
                    temp_row.append(corrcoef)

                # Collect the data into a temporary list
                temp_data.append(temp_row)
                temp_data.append(cond)
                temp_data.append(TA)
                # Add it all into a dataframe row
                row = pd.DataFrame([temp_data], columns = new_cols)
                # Add the row to the total dataframe
                df = pd.concat([row, df], ignore_index = True)
    return df

def classify_SNR(data, TA = None, DT_config = (2, 21)):
    """
    Parameters
    ----------
    data : Pandas dataframe
        Load data from getData().
    TA : Int, optional
        Test subject number. The default is 0.
    tr : Tuple, optional
        Range of max depth values for decision tree classifier. The default is (2,21,1).

    Returns
    -------
    Gaussian process regression with two-layer 15-fold CV. Decision tree classifier trained
    on validation set in inner loops; the DTCs are saved to pickle files. DTC with highest
    accuracy is loaded for evaluation on GPR performance on test data in outer loops.

    """

    # Values for tree max depth
    DT_params = np.arange(DT_config[0],
                          DT_config[1])

    # Define result DataFrame
    df_cols = ["SNR_GP", "SNR_DT", "SNR_true", "kernel", "TA"]
    df = pd.DataFrame(columns = df_cols)

    # For reproduciblity
    random_state = 999

    # Restructure data matrix
    data = transform_to_SNR(data)

    # Convert labels to [-5,0,5] for SNRs -5, 0, +5 DB, respectively
    data["SNR"] = data["SNR"].replace(0, -5)
    data["SNR"] = data["SNR"].replace(1, 0)
    data["SNR"] = data["SNR"].replace(2, 5)

    # Initiate kernels for GPR
    kernels = []

    kernel_lin = DotProduct()
    kernel_sqd = RBF(length_scale = len(data["trial"][0]))
    kernel_sum = kernel_lin + kernel_sqd

    kernels.append(kernel_lin)
    kernels.append(kernel_sqd)
    kernels.append(kernel_sum)

    kernel_labels = ["Linear", "Squared Exponential", "Sum"]

    # Shuffle data
    data = data.sample(frac = 1, random_state = random_state)

    if TA == None:
        TAs = np.unique(data["TA"])
    else:
        TAs = np.array([TA])

    for TA in TAs:
        data_sub = data[data["TA"] == TA]

        X = data_sub["trial"]
        y = data_sub["SNR"]

        ## 2-layer 15-fold cross-validation ##
        kf = KFold(n_splits = 15)
        k_ = 0
        for kernel in kernels:
            # Outer fold
            i = 0
            for out_train_idx, out_test_idx in kf.split(X, y):
                print("TA: %i / %i\tKernel: %i / %i\nOuter fold: %i / %i" %(TA + 1, len(TAs),
                                                                              k_ + 1, len(kernels),
                                                                              i + 1, kf.get_n_splits(X, y)))

                X_train = X.iloc[out_train_idx]
                y_train = y.iloc[out_train_idx]

                X_test = X.iloc[out_test_idx]
                y_test = y.iloc[out_test_idx]

                # Initiate errors for inner fold validations
                vals = np.zeros((kf.get_n_splits(X_train,y_train), len(DT_params)))

                # Inner fold
                j = 0
                for inn_train_idx, inn_test_idx in kf.split(X_train, y_train):
                    print("\tInner fold: %i / %i" %(j + 1, kf.get_n_splits(X_train, y_train)))

                    inn_X_train = X_train.iloc[inn_train_idx]
                    inn_y_train = y_train.iloc[inn_train_idx]

                    inn_X_test = X_train.iloc[inn_test_idx]
                    inn_y_test = y_train.iloc[inn_test_idx]

                    inn_X_train = np.reshape(list(inn_X_train), ((len(inn_X_train), len(X_train.iloc[0]))))
                    inn_y_train = inn_y_train.values.reshape(inn_X_train.shape[0], 1)

                    inn_X_test = np.reshape(list(inn_X_test), ((len(inn_X_test), len(X_test.iloc[0]))))
                    inn_y_test = inn_y_test.values.reshape(inn_X_test.shape[0], 1)

                    GP_model = GaussianProcessRegressor(kernel = kernel, random_state = random_state)
                    GP_model.fit(inn_X_train, inn_y_train)
                    DT_train = GP_model.predict(inn_X_train, return_std=False)
                    DT_test = GP_model.predict(inn_X_test, return_std=False)

                    k = 0
                    # Loop through max depth values for DTC
                    for param in DT_params:
                        # Decision tree classifier
                        DT_model = tree.DecisionTreeClassifier(criterion = 'entropy', max_depth = param)

                        # Fit DTC
                        DT_model.fit(DT_train, inn_y_train)

                        # Test DTC on y_pred for GPR and hold it up against y_test (inner)
                        val = DT_model.score(DT_test, inn_y_test)

                        # Add score to matrix
                        vals[j, k] = val

                        k += 1
                    j += 1

                X_train = np.reshape(list(X_train), ((len(X_train), len(X.iloc[0]))))
                y_train = y_train.values.reshape(X_train.shape[0], 1)

                X_test = np.reshape(list(X_test), ((len(X_test), len(X.iloc[0]))))
                y_test = y_test.values.reshape(X_test.shape[0], 1)

                # Get optimal DTC model
                param_score = np.sum(vals, axis = 0)
                idx = np.argmax(param_score)
                best_param = DT_params[idx]

                opt_DT_model = tree.DecisionTreeClassifier(criterion = 'entropy', max_depth = best_param)

                GP_model = GaussianProcessRegressor(kernel = kernel, random_state = random_state)
                GP_model.fit(X_train, y_train)
                DT_train = GP_model.predict(X_train, return_std = False)
                DT_test = GP_model.predict(X_test, return_std = False)

                opt_DT_model.fit(DT_train, y_train)
                preds = opt_DT_model.predict(DT_test)

                for j in range(len(preds)):
                    # Convert to DataFrame
                    data_results = np.zeros((1, len(df_cols)), dtype="object")
                    data_results[:, 0] = DT_test[j]
                    data_results[:, 1] = preds[j]
                    data_results[:, 2] = y_test[j][0]
                    data_results[:, 3] = kernel_labels[k_]
                    data_results[:, 4] = TA

                    df_ = pd.DataFrame(data = data_results, columns = df_cols)

                    # Concatenate
                    df = pd.concat([df, df_], ignore_index = True)

                i += 1
            k_ += 1
    # Transform categories
    df["kernel"] = df["kernel"].astype("category")
    df["TA"] = df["TA"].astype("category")

    # Save results
    df.to_pickle("local_data/results_GP.pkl")

    # Return results
    return df


if __name__ == "__main__":
    # Set working directory
    chdir(path.dirname(__file__))

    data = getData()

    #results = classify_SNR(data)
    results_ = pd.read_pickle("local_data/results_GP.pkl")

    results_["correct"] = results_["SNR_DT"] == results_["SNR_true"]
    results_["correct"] = results_["correct"].replace(True, 1)
    results_["correct"] = results_["correct"].replace(False, 0)


    results = results_.groupby(['kernel', 'TA']).mean().reset_index()

    sns.set(style="darkgrid")

    # Boxplots per SNR
    sns.boxplot(x='kernel',y='correct',data=results)
    plt.xlabel("Kernel")
    plt.ylabel("Accuracy")
    plt.yticks(plt.yticks()[0], [str(int(tick * 100)) + "%" for tick in plt.yticks()[0]])
    plt.title("Boxplot of Kernel Accuracies")
    #plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.show()

    # Line plots per TA
    #for TA in np.unique(results["TA"]):
        #sns.lineplot(x='TA',y='correct',data=results[results["TA"] == TA], hue = 'kernel')
    sns.lineplot(x='TA',y='correct',data=results_, hue = 'kernel')
    plt.xlabel("TS")
    plt.ylabel("Accuracy")
<<<<<<< HEAD
    plt.title("TS Performance")
    plt.legend(loc = 'upper right', borderaxespad=0.)
    plt.title("Test Subject (TS) Performance")
    #plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
    plt.legend(loc = "upper right")
>>>>>>> 12a77156dc5266e227c153f3fcb2441d91781feb
    plt.show()

    for TA in TAs:
        res_sub = results_[results_["TA"] == TA]
        plt.plot(res_sub[res_sub["kernel"] == "Linear"].reset_index()["SNR_DT"], ".-", label = "GP-DT")
        plt.plot(res_sub[res_sub["kernel"] == "Linear"].reset_index()["SNR_true"], ".-", label = "Target SNR")
        plt.xlabel("Index")
        plt.ylabel("SNR (dB)")
        plt.title("Classification performance (linear kernel), TS = %i" %(TA + 1))
        plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
        plt.savefig("local_data/plots/GP/lineplot_lin_%i.png" %TA, bbox_inches='tight')
        plt.show()

    for TA in TAs:
        res_sub = results_[results_["TA"] == TA]
        plt.plot(res_sub[res_sub["kernel"] == "Squared Exponential"].reset_index()["SNR_DT"], ".-", label = "GP-DT")
        plt.plot(res_sub[res_sub["kernel"] == "Squared Exponential"].reset_index()["SNR_true"], ".-", label = "Target SNR")
        plt.xlabel("Index")
        plt.ylabel("SNR (dB)")
        plt.title("Classification performance (squared exponential kernel), TS = %i" %(TA+ 1 ))
        plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
        plt.savefig("local_data/plots/GP/lineplot_sqd_%i.png" %TA, bbox_inches='tight')
        plt.show()
