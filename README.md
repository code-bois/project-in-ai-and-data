# Project in AI and Data
## About
This is a repository for the files used in 02466 Project Work - Bachelor in AI and Data.

## Installation
Simply copy the files to a desired location.

Download our data from https://files.dtu.dk/userportal/#/files/CL%20fagpakkeprojekt.

Data is in .pkl files, they must be saved under **src/local_data**.

## Your own data

To use your own data and our preprocessing, save the data in the following paths:

EEG data has to be saved in the directory: **src/local_data/EEG**.

Audio data has to be saved in the directory: **src/local_data/audio**.

## Notes
Any folder prefixed local will be ignored by the repository.

## Pickle for EEG
Download the pickle file for our EEG data from:
https://app.box.com/s/lwi57i68j9x2ojbhrgz71vzrbtp9d9fo 

## Pickle Files for Models with Results
###### L2-regularized ALL SNR:
https://files.dtu.dk/u/hlYjlDXWI5mUViOS/results_all_SNR.pkl?l

###### L2-regularized Seperate SNR:
https://files.dtu.dk/u/6Pn03Ft59Nb6-fTa/result_from_separate_SNR.pkl?l

###### ANN 1 Hidden FC All SNR:
https://files.dtu.dk/u/xtCrIHVQqyDU0Ksm/ALL_SNR_1_layer_ANN_result.pkl?l 

###### ANN 2 Hidden FCs All SNR:
https://files.dtu.dk/u/hTzDoTa8w6CXPMOn/ALL_SNR_2_layer_ANN_result.pkl?l

###### ANN 1 Hidden FC Seperate SNR:
https://files.dtu.dk/u/-LG7iEsxu4bqsbuO/Seperate_SNR_1_layer_ANN_results.pkl?l

###### ANN 2 Hidden FCs Seperate SNR:
https://files.dtu.dk/u/yi00gXmtNI7YLTSH/Separate%20SNR%20ANN%202%20Layer%20Result?l

###### GPR Model:
https://files.dtu.dk/u/6-rk7lhq5OL56uTo/results_GP.pkl?l

